package net.verycook.web.test;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestServiceImpl implements TestService {
	
	@Autowired
	TestDAO testDao;
	
	Logger logger = Logger.getLogger(getClass());
	
	@Override
	public TestDTO getTestId() {
		TestDTO dto = new TestDTO();
		dto.setTestid(testDao.getTestId(1));
		logger.debug("아이디 : " + dto.getTestid());
		return dto;
	}

}
