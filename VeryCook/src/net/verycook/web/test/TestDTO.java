package net.verycook.web.test;

public class TestDTO {
	private int testno;
	private String testid;
	
	public TestDTO() {
		// TODO Auto-generated constructor stub
	}
	
	public void setTestno(int testno) {
		this.testno = testno;
	}
	
	public int getTestno() {
		return testno;
	}
	
	public void setTestid(String testid) {
		this.testid = testid;
	}
	
	public String getTestid() {
		return testid;
	}
}
