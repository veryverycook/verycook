package net.verycook.web.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class TestController {
	
	@Autowired
	TestService testService;
	
	@RequestMapping(value="/test")
	public String testView(Model model) {
		model.addAttribute("dto", testService.getTestId());
		
		return "test";
	}
	
}
