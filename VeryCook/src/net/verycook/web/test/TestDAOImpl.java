package net.verycook.web.test;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TestDAOImpl implements TestDAO {
	
	@Autowired
	SqlSession sqlSession;
	
	@Override
	public String getTestId(int testno) {
		
		return sqlSession.selectOne("net.verycook.web.test.getTestId", testno);
	}

}
