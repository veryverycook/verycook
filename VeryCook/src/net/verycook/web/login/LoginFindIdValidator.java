package net.verycook.web.login;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class LoginFindIdValidator implements Validator {

	LoginDAO dao;
	
	public LoginFindIdValidator(LoginDAO dao) {
		this.dao=dao;
		
	}
	@Override
	public boolean supports(Class<?> clazz) {
		return LoginDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		LoginDTO dto = (LoginDTO)target;
		
		String memberEmail=dao.selectEmail(dto.getMemberPhone());
		
		if(dto.getMemberPhone()==""||memberEmail==null){
			errors.rejectValue("memberPhone","memberPhone.required2");
		}else{
			String star="";
			for(int i=0;i<memberEmail.substring(3, memberEmail.indexOf("@")).length();i++){
				star+="*";
			}
			
			String id=memberEmail.replaceFirst(memberEmail.substring(3, memberEmail.indexOf("@")),star);
			
			dto.setMemberEmail(id);
			
			
		}
		

		
	}

}
