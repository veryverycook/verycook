package net.verycook.web.login;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class LoginController {
	@Autowired
	LoginDAO dao;
	
	@RequestMapping(value="/login/login")
	public String loginlogin(Model model){
		LoginDTO dto = new LoginDTO();
		model.addAttribute("dto", dto);
		
		return "/login/login";
	}
	@RequestMapping(value="/login/loginOk")
	public String loginloginOk(@ModelAttribute("dto")LoginDTO dto ,BindingResult result, Model model,HttpServletRequest req){
		new LoginValidator(dao).validate(dto, result);
		
		if(result.hasErrors()){
			dto.setIsWrong("false");
			model.addAttribute("dto", dto);
			return"login/login";
		
		}
		
		dto.setIsWrong("true");
		if(dto.getMemberStats()==99){
			return "/admin/admin";
			
		}else{
			req.getSession().setAttribute("MemberEmail", dto.getMemberEmail());
			req.getSession().setAttribute("MemberName", dao.selectName(dto.getMemberEmail()));
			req.getSession().setAttribute("MemberStats", dao.selectStats(dto.getMemberEmail()));
			
			return "redirect:/mainlist/mainlist";
		}
		
		
	}
	
	@RequestMapping(value="/login/findid")
	public String findid(Model model){
		LoginDTO dto = new LoginDTO();
		model.addAttribute("dto", dto);
		return "/login/findid";
	}
	
	@RequestMapping(value="/login/findidOk")
	public String findidOk(@ModelAttribute("dto")LoginDTO dto,BindingResult result, Model model){
		
		new LoginFindIdValidator(dao).validate(dto, result);
		if(result.hasErrors()){
			dto.setIsWrong("false");
			model.addAttribute("dto", dto);
			return"login/findid";
		
		}
		dto.setIsWrong("true");
		model.addAttribute("dto", dto);
		
		
		return "login/findidOk";
	}
	@RequestMapping(value="/login/findpassword")
	public String findpassword(Model model){
		LoginDTO dto = new LoginDTO();
		model.addAttribute("dto", dto);
		return "/login/findpassword";
	}
	
	@RequestMapping(value="/login/findpasswordOk")
	public String findpassword(@ModelAttribute("dto")LoginDTO dto,BindingResult result, Model model){
		
		new LoginFindPasswordValidator(dao).validate(dto, result);
		
		if(result.hasErrors()){
			dto.setIsWrong("false");
			model.addAttribute("dto", dto);
			return"login/findpassword";
		
		}
		dto.setIsWrong("true");
		
		
		
		return "/login/findpasswordOk";
	}
	

}
