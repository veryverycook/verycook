package net.verycook.web.login;

public class LoginDTO {
	
	private String memberEmail;
	private String memberPassword;
	private String memberRpassword;
	private String memberName;
	private int memberScore;
	private String memberJoindate;
	private String year;
	private String month;
	private String toDay;
	private String memberBirth;
	private char memberGender;
	private int memberStats;
	private String memberPhone;
	private String isWrong;
	public LoginDTO() {
		// TODO Auto-generated constructor stub
	}
	
	
	public LoginDTO(String memberEmail, String memberPassword,
			String memberRpassword, String memberName, int memberScore,
			String memberJoindate, String year, String month, String toDay,
			String memberBirth, char memberGender, int memberStats,
			String memberPhone, String isWrong) {
		super();
		this.memberEmail = memberEmail;
		this.memberPassword = memberPassword;
		this.memberRpassword = memberRpassword;
		this.memberName = memberName;
		this.memberScore = memberScore;
		this.memberJoindate = memberJoindate;
		this.year = year;
		this.month = month;
		this.toDay = toDay;
		this.memberBirth = memberBirth;
		this.memberGender = memberGender;
		this.memberStats = memberStats;
		this.memberPhone = memberPhone;
		this.isWrong = isWrong;
	}


	public String getMemberEmail() {
		return memberEmail;
	}
	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}
	public String getMemberPassword() {
		return memberPassword;
	}
	public void setMemberPassword(String memberPassword) {
		this.memberPassword = memberPassword;
	}
	public String getMemberRpassword() {
		return memberRpassword;
	}
	public void setMemberRpassword(String memberRpassword) {
		this.memberRpassword = memberRpassword;
	}
	public String getMemberName() {
		return memberName;
	}
	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}
	public int getMemberScore() {
		return memberScore;
	}
	public void setMemberScore(int memberScore) {
		this.memberScore = memberScore;
	}
	public String getMemberJoindate() {
		return memberJoindate;
	}
	public void setMemberJoindate(String memberJoindate) {
		this.memberJoindate = memberJoindate;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public String getToDay() {
		return toDay;
	}
	public void setToDay(String toDay) {
		this.toDay = toDay;
	}
	public String getMemberBirth() {
		return memberBirth;
	}
	public void setMemberBirth(String memberBirth) {
		this.memberBirth = memberBirth;
	}
	public char getMemberGender() {
		return memberGender;
	}
	public void setMemberGender(char memberGender) {
		this.memberGender = memberGender;
	}
	public int getMemberStats() {
		return memberStats;
	}
	public void setMemberStats(int memberStats) {
		this.memberStats = memberStats;
	}
	public String getMemberPhone() {
		return memberPhone;
	}
	public void setMemberPhone(String memberPhone) {
		this.memberPhone = memberPhone;
	}
	public String getIsWrong() {
		return isWrong;
	}
	public void setIsWrong(String isWrong) {
		this.isWrong = isWrong;
	}
	
	
	
	
}
