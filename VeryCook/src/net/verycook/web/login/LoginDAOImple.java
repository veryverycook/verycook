package net.verycook.web.login;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LoginDAOImple implements LoginDAO{

	@Autowired
	SqlSession sqlsession;
	
	@Override
	public String selectOne(LoginDTO dto) {
		
		return sqlsession.selectOne("net.verycook.web.login.selectOne",dto);
	}

	@Override
	public String selectEmail(String memberPhone) {
		
		return sqlsession.selectOne("net.verycook.web.login.selectEmail",memberPhone);
	}

	@Override
	public String selectPassword(String memberEmail) {
		
		return sqlsession.selectOne("net.verycook.web.login.selectPassword",memberEmail);
	}

	@Override
	public String selectName(String memberEmail) {
		
		return sqlsession.selectOne("net.verycook.web.login.selectName",memberEmail);
	}

	@Override
	public int selectStats(String memberEmail) {
		
		return sqlsession.selectOne("net.verycook.web.login.selectStats",memberEmail);
	}

}
