package net.verycook.web.login;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import net.verycook.web.login.LoginDTO;

public class LoginJavaMail {


	public LoginJavaMail(LoginDTO dto) {
		

		String host = "smtp.gmail.com";
		String username = "verycook12@gmail.com";
		String password = "verycook1234";

		// 메일 내용
		String recipient = dto.getMemberEmail();
		String subject = "VeryCook 비밀번호 메일입니다.^^	";
		String body = "당신의 비밀번호는 : "+dto.getMemberPassword();
		// properties 설정
		Properties props = new Properties();
		props.put("mail.smtps.auth", "true");
		// 메일 세션
		Session session = Session.getDefaultInstance(props);
		MimeMessage msg = new MimeMessage(session);

		// 메일 관련
		try {
			msg.setSubject(subject);

			msg.setText(body);
			msg.setFrom(new InternetAddress(username));
			msg.addRecipient(Message.RecipientType.TO, new InternetAddress(
					recipient));

			// 발송 처리
			Transport transport = session.getTransport("smtps");
			transport.connect(host, username, password);
			transport.sendMessage(msg, msg.getAllRecipients());
			transport.close();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
