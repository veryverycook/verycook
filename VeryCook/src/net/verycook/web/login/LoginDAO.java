package net.verycook.web.login;

public interface LoginDAO {
	
	public String selectOne(LoginDTO dto);
	public String selectEmail(String memberPhone);
	public String selectPassword(String memberEmail);
	public String selectName(String memberEmail);
	public int selectStats(String memberEmail);
}
