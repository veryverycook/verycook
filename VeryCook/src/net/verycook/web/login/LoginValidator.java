package net.verycook.web.login;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class LoginValidator implements Validator {

	LoginDAO dao;
	
	public LoginValidator(LoginDAO dao) {
		this.dao=dao;
		
	}
	
	
	
	@Override
	public boolean supports(Class<?> clazz) {
		return LoginDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "memberPassword","memberPassword.wrong");
		LoginDTO dto = (LoginDTO)target;
		

		String memberStats=dao.selectOne(dto);
		
		System.out.println("memberStats"+memberStats);
		System.out.println("dto"+dto);
		
		if(memberStats==null||dto.getMemberEmail()==null||memberStats.equals("0")){
			errors.rejectValue("memberEmail","memberEmail.wrong");
		}else{
			int memberstats=Integer.parseInt(memberStats);
			dto.setMemberStats(memberstats);
		}
		

	}

}
