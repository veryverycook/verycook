package net.verycook.web.login;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class LoginFindPasswordValidator implements Validator {

	LoginDAO dao;

	public LoginFindPasswordValidator(LoginDAO dao) {
		this.dao = dao;

	}

	@Override
	public boolean supports(Class<?> clazz) {
		return LoginDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		LoginDTO dto = (LoginDTO)target;
		String memberPassword=dao.selectPassword(dto.getMemberEmail());
		
		if(dto.getMemberEmail()==""||memberPassword==null){
			errors.rejectValue("memberPhone","memberPhone.required2");
		}else{
			dto.setMemberPassword(memberPassword);
			new LoginJavaMail(dto);
			
		}
		
		
	}

}
