package net.verycook.web.join;

import net.verycook.web.login.LoginDTO;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class JoinDAOImple implements JoinDAO {

	
	@Autowired
	SqlSession sqlSession;
	
	
	@Override
	public String selectOne(String memberEmail) {
		
		return sqlSession.selectOne("net.verycook.web.join.selectOne", memberEmail);
	}


	@Override
	public void insertOne(LoginDTO dto) {
		sqlSession.insert("net.verycook.web.join.insertOne", dto);
	}


	@Override
	public void updateOne(String memberEmail) {
		sqlSession.update("net.verycook.web.join.updateOne", memberEmail);
	}

}
