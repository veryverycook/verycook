package net.verycook.web.join;

import net.verycook.web.login.LoginDTO;

public interface JoinDAO {
	public String selectOne(String memberEmail);
	public void insertOne(LoginDTO dto);
	public void updateOne(String memberEmail);
}
