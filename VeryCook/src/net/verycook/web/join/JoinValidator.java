package net.verycook.web.join;

import net.verycook.web.login.LoginDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class JoinValidator implements Validator{

	JoinDAO dao;
	
	public JoinValidator(JoinDAO dao) {
		this.dao = dao;
	}
	
	
	@Override
	public boolean supports(Class<?> clazz) {
		// TODO Auto-generated method stub
		return LoginDTO.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "memberName", "memberName.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "memberEmail", "memberEmail.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "memberPassword", "memberPassword.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "memberRpassword", "memberRpassword.required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "memberPhone", "memberPhone.required");
		
		LoginDTO dto =(LoginDTO)target;
		String result=null;
		try{
			
			result=dao.selectOne(dto.getMemberEmail());
		}catch(NullPointerException e){
			
		}
		if(result!=null){
			errors.rejectValue("memberEmail","memberEmail.overlap");
		}
		
		if(!dto.getMemberPassword().equals(dto.getMemberRpassword())){
			errors.rejectValue("memberRpassword","memberRpassword.wrong");
		}
		
		if(dto.getYear().equals("연도") || dto.getMonth().equals("월")||dto.getToDay().equals("일")){
			errors.rejectValue("toDay","toDay.aaa");
		}
		
		
		
		
	}

}
