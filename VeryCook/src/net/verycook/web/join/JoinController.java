package net.verycook.web.join;

import net.verycook.web.login.LoginDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class JoinController {
	@Autowired
	JoinDAO dao;
	
	@RequestMapping(value="join/join")
	public String join(Model model){
		LoginDTO dto = new LoginDTO();
		model.addAttribute("dto",dto);
		return "/join/join";
}
	
	@RequestMapping(value="join/joinemail")
	public String joinEmail(@RequestParam("email")String email){
		LoginDTO dto= new LoginDTO();
		dto.setMemberEmail(email);
		
		new JoinJavaMail(dto);
		return "/join/joinings";
	}
		
	
	@RequestMapping(value="join/joining")
	public String joining(@ModelAttribute(value="dto")LoginDTO dto,BindingResult result, Model model){
		
		new JoinValidator(dao).validate(dto, result);
		if(result.hasErrors()){
			System.out.println("에러있음 ");
			return "/join/join";
		}
		
		String birth=dto.getYear()+dto.getMonth()+dto.getToDay();
		dto.setMemberBirth(birth);
		
		dao.insertOne(dto);
		new JoinJavaMail(dto);
		
		model.addAttribute("eemail", dto.getMemberEmail());
		
		
		
		return "/join/joinings";
	}
	@RequestMapping(value="join/joinOk")
	public String joinOk(@RequestParam(value="memberEmail")String memberEmail){
		
		dao.updateOne(memberEmail);
		
		
		return "/join/joinOk";
	}

	

}
