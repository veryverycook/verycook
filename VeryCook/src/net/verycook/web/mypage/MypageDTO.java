package net.verycook.web.mypage;

public class MypageDTO {
	//커밋을위한주석	
	private int recipeNo;
	private String recipeContents;
	private int recipeRecommend;
	private int follewNo;
	private String imageSrc;
	private String memberEmail;
	private int memberSore;
	
	public MypageDTO(){}

	public MypageDTO(int recipeNo, String recipeContents, int recipeRecommend,
			int follewNo, String imageSrc, String memberEmail, int memberSore) {
		super();
		this.recipeNo = recipeNo;
		this.recipeContents = recipeContents;
		this.recipeRecommend = recipeRecommend;
		this.follewNo = follewNo;
		this.imageSrc = imageSrc;
		this.memberEmail = memberEmail;
		this.memberSore = memberSore;
	}

	public int getMemberSore() {
		return memberSore;
	}
	
	public void setMemberSore(int memberSore) {
		this.memberSore = memberSore;
	}

	public int getRecipeNo() {
		return recipeNo;
	}

	public void setRecipeNo(int recipeNo) {
		this.recipeNo = recipeNo;
	}

	public String getRecipeContents() {
		return recipeContents;
	}

	public void setRecipeContents(String recipeContents) {
		this.recipeContents = recipeContents;
	}

	public int getRecipeRecommend() {
		return recipeRecommend;
	}

	public void setRecipeRecommend(int recipeRecommend) {
		this.recipeRecommend = recipeRecommend;
	}

	public int getFollewNo() {
		return follewNo;
	}

	public void setFollewNo(int follewNo) {
		this.follewNo = follewNo;
	}

	public String getImageSrc() {
		return imageSrc;
	}

	public void setImageSrc(String imageSrc) {
		this.imageSrc = imageSrc;
	}

	public String getMemberEmail() {
		return memberEmail;
	}

	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}

	
	
}
