package net.verycook.web.mypage;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MypageController {
	//커밋을위한주석
	@Autowired
	MypageService mypageService;

	Logger logger = Logger.getLogger(getClass());

	@RequestMapping(value = "/mypage/mypage")
	public String mypageView(Model model, HttpServletRequest request) {
		// HttpSession session = request.getSession();
		// String email = (String) session.getAttribute("email");
		String email = "sunguk7704@naver.com";
		model.addAttribute("memberEmail", email);
		model.addAttribute("recipeCnt", mypageService.getRecipeTotal(email));
		model.addAttribute("followCnt", mypageService.getFollowTotal(email));
		model.addAttribute("followerCnt", mypageService.getFollowerTotal(email));
		model.addAttribute("memberScore", mypageService.getMemberScore(email));

		model.addAttribute("recipe", mypageService.getRecipe(email));
		model.addAttribute("recipeRecommand",
				mypageService.getRecipeRecommend(email));
		model.addAttribute("recipeImage", mypageService.getRecipeImage(6));

		logger.debug("레시피갯수 : " + mypageService.getRecipeTotal(email));
		logger.debug("팔로잉갯수 : " + mypageService.getFollowTotal(email));
		logger.debug("팔로우갯수 : " + mypageService.getFollowerTotal(email));

		return "/mypage/mypage";

	}

	@RequestMapping(value = "/mypage/modify")
	public String modifyView() {
		
		return "redirect:/mypage/Membermodify";
	}
}
