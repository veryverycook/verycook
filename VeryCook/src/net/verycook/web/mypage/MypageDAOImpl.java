package net.verycook.web.mypage;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
//커밋을위한주석
@Repository
public class MypageDAOImpl implements MypageDAO{
	@Autowired
	SqlSession sqlSession;

	@Override
	public int getRecipeTotal(String memberEmail) {
		// 어디 매퍼에 어떤 sql문을 실행할것인지 주소를 줘야함
		return sqlSession.selectOne("net.verycook.web.mypage.getRecipeTotal", memberEmail);
	}

	@Override
	public int getFollowTotal(String memberEmail) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("net.verycook.web.mypage.getfollowTotal", memberEmail);
	}

	@Override
	public int getFollowerTotal(String memberEmail) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("net.verycook.web.mypage.getfollowerTotal", memberEmail);
	}

	@Override
	public List<String> getRecipe(String memberEmail) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("net.verycook.web.mypage.getrecipe", memberEmail);
		
	}

	@Override
	public List<String> getRecipeImage(int recipeNo) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("net.verycook.web.mypage.getrecipeImage", recipeNo);
	}

	@Override
	public List<Integer> getRecipeRecommend(String memberEmail) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("net.verycook.web.mypage.getRecipeRecommend", memberEmail);
	}

	@Override
	public int getMemberScore(String memberEmail) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("net.verycook.web.mypage.getMemberScore", memberEmail);
	}


}
