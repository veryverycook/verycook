package net.verycook.web.mypage;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
//커밋을위한주석
@Service
public class MypageServiceImpl implements MypageService {
	@Autowired
	MypageDAO mypagedao;

	Logger logger = Logger.getLogger(getClass());

	@Override
	public int getRecipeTotal(String memberEmail) {
		logger.debug("이메일 : " + memberEmail);
		return mypagedao.getRecipeTotal(memberEmail);
	}

	@Override
	public int getFollowTotal(String memberEmail) {
		return mypagedao.getFollowTotal(memberEmail);
	}

	@Override
	public int getFollowerTotal(String memberEmail) {

		return mypagedao.getFollowerTotal(memberEmail);
	}

	@Override
	public List<String> getRecipe(String memberEamil) {
		return mypagedao.getRecipe(memberEamil);
	}

	@Override
	public List<String> getRecipeImage(int recipeNo) {
		// TODO Auto-generated method stub
		return mypagedao.getRecipeImage(recipeNo);
	}

	@Override
	public List<Integer> getRecipeRecommend(String memberEmail) {
		// TODO Auto-generated method stub
		return mypagedao.getRecipeRecommend(memberEmail);
	}

	@Override
	public int getMemberScore(String memberEmail) {
		// TODO Auto-generated method stub
		return mypagedao.getMemberScore(memberEmail);
	}

}
