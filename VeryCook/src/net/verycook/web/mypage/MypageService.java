package net.verycook.web.mypage;

import java.util.List;
//커밋을위한주석
public interface MypageService {
	public int getRecipeTotal(String memberEmail);
	public int getFollowTotal(String memberEmail);
	public int getFollowerTotal(String memberEmail);
	public int getMemberScore(String memberEmail);
	public List<String> getRecipe(String memberEamil);
	public List<String> getRecipeImage(int recipeNo);
	public List<Integer> getRecipeRecommend(String memberEmail);
}
