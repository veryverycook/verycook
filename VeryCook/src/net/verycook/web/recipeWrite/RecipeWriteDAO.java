package net.verycook.web.recipeWrite;

import java.util.List;

public interface RecipeWriteDAO {
	public void inputRecipe(RecipeWriteDTO dto);
	public void inputRecipeImg(String img);
	public void inputRecipeNewTags(String tag);
	public void inputRecipeTag(int tagNum);
	public List<String> selectRecipeTags();
	public int selectRecipeTagNum(String tagName);
	
}
