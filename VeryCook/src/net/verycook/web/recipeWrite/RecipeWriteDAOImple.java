package net.verycook.web.recipeWrite;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RecipeWriteDAOImple implements RecipeWriteDAO{
	
	@Autowired
	SqlSession sqlSession;

	@Override
	public void inputRecipe(RecipeWriteDTO dto) {
		// TODO Auto-generated method stub
		sqlSession.insert("net.verycook.web.recipeWrite.inputRecipe", dto);
	}

	@Override
	public void inputRecipeImg(String img) {
		// TODO Auto-generated method stub
		sqlSession.insert("net.verycook.web.recipeWrite.inputRecipeImg", img);
	}

	@Override
	public void inputRecipeNewTags(String tag) {
		// TODO Auto-generated method stub
		sqlSession.insert("net.verycook.web.recipeWrite.inputRecipeNewTags", tag);
	}

	@Override
	public List<String> selectRecipeTags() {
		// TODO Auto-generated method stub
		return sqlSession.selectList("net.verycook.web.recipeWrite.selectRecipeTags");
	}

	@Override
	public void inputRecipeTag(int tagNum) {
		// TODO Auto-generated method stub
		sqlSession.insert("net.verycook.web.recipeWrite.inputRecipeTag", tagNum);
	}

	@Override
	public int selectRecipeTagNum(String tagName) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("net.verycook.web.recipeWrite.selectRecipeTagNum", tagName);
	}

}
