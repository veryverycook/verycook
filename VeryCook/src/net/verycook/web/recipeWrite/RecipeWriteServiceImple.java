package net.verycook.web.recipeWrite;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.verycook.web.test.TestDTO;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Service
public class RecipeWriteServiceImple implements RecipeWriteService{

	@Autowired
	RecipeWriteDAO dao;
	
	Logger logger = Logger.getLogger(getClass());
	
	@Override
	public void inputRecipe(RecipeWriteDTO dto) {
		// TODO Auto-generated method stub
		
		dao.inputRecipe(dto);
		logger.debug("dto.getRecipeContents() : " + dto.getRecipeContents());
	}

	@Override
	public void inputRecipeImg(RecipeWriteDTO dto, HttpServletRequest req) {
		// TODO Auto-generated method stub
		
		// logger.debug("dto.getImgSrc() : " + dto.getImgSrc());
		
		List<MultipartFile> mf = dto.getUploadFile();
		
		// /WEB-INF/uploadPath 디렉토리의 절대경로
		HttpSession hs = req.getSession();
		ServletContext app = hs.getServletContext();
		String filePath = app.getRealPath("/WEB-INF/uploadFile");
		System.out.println("파일이 저장될 실제 경로 : "+filePath);
		List<String> imgSrc = new ArrayList<String>();
		// 파일이름
		for (int i = 0; i < mf.size(); i++) {
			String fileName = mf.get(i).getOriginalFilename();
			System.out.println("fileName : "+fileName);
			imgSrc.add(i, fileName);
			int count = 0;
			// 만약 동일한 파일이 존재한다면 파일의 이름을 변형
			// abc.jpg
			File f = new File(filePath + "/" + fileName);
			
			/*while (f.exists()) {
				count++;
				String[] filePiece = fileName.split("\\.");
				f = new File(filePath + "/" + filePiece[0] + "(" + count + ")"
						+ "." + filePiece[1]);
				// c:\~~~~~~~~~~~~~~~~~~/abc(1).jpg
			}*/
			
			System.out.println(imgSrc);

			// 임시 저장소에 보관되어 있는 파일을 저장하고자 하는 경로 복사
			try {
				mf.get(i).transferTo(f);
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		dto.setImgSrc(imgSrc);
		List<String> abc = dto.getImgSrc(); 
		for(int i=0; i<abc.size(); i++) {
			dao.inputRecipeImg(abc.get(i));
		}
		
	}

	@Override
	public void inputRecipeTags(String recipeContents) {
		// TODO Auto-generated method stub
		// System.out.println(recipeContents);
		
		
		// String recipeContents = "안녕하세요 오늘 올릴 #레시피는 #고구마 #말랭이 #베리쿡 입니다. 재료는 바로 #감자#고구마#딸기 입니다. #쿡베리";
		
		System.out.println("==========================");

		String temp = recipeContents;
		String nextstr = "";
		List<String> newTag = new ArrayList<String>();
		int start;
		int end;
		int end1;
		while(temp.indexOf("#")!=-1){
			start = temp.indexOf("#");
			nextstr = temp.substring(start+1);
			// System.out.println(nextstr);
			
			// 구간 자르기
			end = nextstr.indexOf(" ");
			end1 = nextstr.indexOf("#");
	
			if(end<end1 && end!=-1 && end1!=-1){
				newTag.add(nextstr.substring(0, end));
				temp=nextstr.substring(end);
			}else if(end>end1 && end!=-1 && end1!=-1){
				newTag.add(nextstr.substring(0, end1));
				temp=nextstr.substring(end1);
			}else if(end>0 && end1==-1){
				newTag.add(nextstr.substring(0, end));
				temp=nextstr.substring(end);
			}else if(end==-1 && end1==-1){
				newTag.add(nextstr.substring(0));
				temp=nextstr.substring(0);
			}else break;
			
		}		
		// System.out.println(newTag);
		
		List<String> tempTag = new ArrayList<String>();
		List<String> tags = dao.selectRecipeTags();
		System.out.println("현재 존재하는 tag :"+tags);
		
		// System.out.println(tags.indexOf(newTag.get(3)));
			
			
		int tagNum;
		for(int i = 0; i<newTag.size(); i++){
			// 기존에 태그가 존재
				if(tags.indexOf(newTag.get(i))!=-1){
					// recipe_tag 추가
					tagNum = dao.selectRecipeTagNum(tags.get(tags.indexOf(newTag.get(i))));
					dao.inputRecipeTag(tagNum);
				}else{
			// 새로운 태그 추가		
					tempTag.add(newTag.get(i));
					// tag 추가
					dao.inputRecipeNewTags(newTag.get(i));
					// recipe_tag 추가
					tagNum = dao.selectRecipeTagNum(newTag.get(i));
					dao.inputRecipeTag(tagNum);
				}
			
				//System.out.println(tagNum);
		}
		
		
		System.out.println("앞으로 추가될 tag : "+tempTag);
		
		// dao.inputRecipeTags(tag);
		// logger.debug("tag : " + tag);
		
	}
}
	