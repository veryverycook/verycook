package net.verycook.web.recipeWrite;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;


public interface RecipeWriteService {
	public void inputRecipe(RecipeWriteDTO dto);
	public void inputRecipeImg(RecipeWriteDTO dto, HttpServletRequest req);
	public void inputRecipeTags(String tag);

}
