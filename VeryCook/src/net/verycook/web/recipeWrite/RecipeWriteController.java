package net.verycook.web.recipeWrite;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RecipeWriteController {
	
	@Autowired
	RecipeWriteService recipeWriteService;
	
	
	@RequestMapping(value="/recipe/write", method=RequestMethod.GET)
	public String writeView(Model model) {
		// model.addAttribute("dto", testService.getTestId());
		
		return "recipe/writeForm";
	}
	
	@RequestMapping(value="/recipe/write", method=RequestMethod.POST)
	@Transactional
	public String writeOk(@ModelAttribute("dto")RecipeWriteDTO dto, Model model, HttpServletRequest req) {
		// model.addAttribute("dto", recipeWriteService.inputRecipe(dto));
		
		recipeWriteService.inputRecipe(dto);
		recipeWriteService.inputRecipeImg(dto,req);
		recipeWriteService.inputRecipeTags(dto.getRecipeContents());
		
		return "recipe/writeOk";
	}
	

}
