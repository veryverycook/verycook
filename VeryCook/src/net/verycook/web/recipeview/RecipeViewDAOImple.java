package net.verycook.web.recipeview;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RecipeViewDAOImple implements RecipeViewDAO {
	
	@Autowired
	SqlSession sqlSession;

	@Override
	public RecipeViewDTO viewRecipeOne(int recipeNo) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("net.verycook.web.recipeview.selectRecipeOne", recipeNo);
	}

	@Override
	public List<String> viewRecipeImgs(int recipeNo) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("net.verycook.web.recipeview.selectRecipeImgs", recipeNo);
	}

	@Override
	public List<Integer> viewRecipeTagNo(int recipeNo) {
		// TODO Auto-generated method stub
		return sqlSession.selectList("net.verycook.web.recipeview.selectRecipeTagNo", recipeNo);
	}

	@Override
	public String viewRecipeTags(int tagNo) {
		// TODO Auto-generated method stub
		return sqlSession.selectOne("net.verycook.web.recipeview.selectRecipeTags", tagNo);
	}

}
