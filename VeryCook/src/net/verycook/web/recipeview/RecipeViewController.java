package net.verycook.web.recipeview;

import javax.servlet.http.HttpServletRequest;

import net.verycook.web.recipeWrite.RecipeWriteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RecipeViewController {
	
	@Autowired
	RecipeViewService recipeViewService;
	
	
	@RequestMapping(value="/recipe/viewOne")
	public String recipeView(Model model, HttpServletRequest request) {
		int recipeNo = 52;
		model.addAttribute("recipe", recipeViewService.viewRecipeOne(recipeNo));
		model.addAttribute("images", recipeViewService.viewRecipeImgs(recipeNo));
		model.addAttribute("tags", recipeViewService.viewRecipeTags(recipeNo));
		return "recipe/recipeViewOne";
	}

}
