package net.verycook.web.recipeview;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

public class RecipeViewDTO {
	
	private int recipeNo;
	private String recipeContents;
	private String recipeDate;
	private int recipeRecom;
	private int recipeReport;
	private int recipeStatus;
	private String memberEmail;
	
	public RecipeViewDTO(){
		
	}
	
	public RecipeViewDTO(int recipeNo, String recipeContents,
			String recipeDate, int recipeRecom, int recipeReport,
			int recipeStatus, String memberEmail) {
		super();
		this.recipeNo = recipeNo;
		this.recipeContents = recipeContents;
		this.recipeDate = recipeDate;
		this.recipeRecom = recipeRecom;
		this.recipeReport = recipeReport;
		this.recipeStatus = recipeStatus;
		this.memberEmail = memberEmail;
	}

	public int getRecipeNo() {
		return recipeNo;
	}

	public void setRecipeNo(int recipeNo) {
		this.recipeNo = recipeNo;
	}

	public String getRecipeContents() {
		return recipeContents;
	}

	public void setRecipeContents(String recipeContents) {
		this.recipeContents = recipeContents;
	}

	public String getRecipeDate() {
		return recipeDate;
	}

	public void setRecipeDate(String recipeDate) {
		this.recipeDate = recipeDate;
	}

	public int getRecipeRecom() {
		return recipeRecom;
	}

	public void setRecipeRecom(int recipeRecom) {
		this.recipeRecom = recipeRecom;
	}

	public int getRecipeReport() {
		return recipeReport;
	}

	public void setRecipeReport(int recipeReport) {
		this.recipeReport = recipeReport;
	}

	public int getRecipeStatus() {
		return recipeStatus;
	}

	public void setRecipeStatus(int recipeStatus) {
		this.recipeStatus = recipeStatus;
	}

	public String getMemberEmail() {
		return memberEmail;
	}

	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}

	
}
