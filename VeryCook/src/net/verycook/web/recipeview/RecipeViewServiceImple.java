package net.verycook.web.recipeview;

import java.util.ArrayList;
import java.util.List;

import net.verycook.web.recipeWrite.RecipeWriteDAO;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RecipeViewServiceImple implements RecipeViewService{

	@Autowired
	RecipeViewDAO dao;
	
	Logger logger = Logger.getLogger(getClass());

	@Override
	public RecipeViewDTO viewRecipeOne(int recipeNo) {
		// TODO Auto-generated method stub
		RecipeViewDTO dto = new RecipeViewDTO();
		System.out.println(dao.viewRecipeOne(recipeNo));
		dto = dao.viewRecipeOne(recipeNo);
		logger.debug("dto.getRecipeNo() : "+dto.getRecipeNo() );
		logger.debug("dto.getRecipeContents() : " +dto.getRecipeContents() );
		return dto;
	}

	@Override
	public List<String> viewRecipeImgs(int recipeNo) {
		// TODO Auto-generated method stub
		List<String> imgList = dao.viewRecipeImgs(recipeNo);
		// logger.debug("dto.getRecipeContents() : " );
		return imgList;
	}

	@Override
	public List<String> viewRecipeTags(int recipeNo) {
		// TODO Auto-generated method stub
		List<Integer> tagNoList = dao.viewRecipeTagNo(recipeNo);
		List<String> tagList = new ArrayList<String>();
		String tag = "";
		for(int i=0; i<tagNoList.size();i++){
			tag = dao.viewRecipeTags(tagNoList.get(i));
			System.out.println(tagNoList.get(i));
			System.out.println(tag);
			tagList.add(i, tag);
		}
		System.out.println("tagList : "+tagList);
		
		return tagList;
	}
	
}
