package net.verycook.web.recipeview;

import java.util.List;

public interface RecipeViewDAO {
	public RecipeViewDTO viewRecipeOne(int recipeNo);
	public List<String> viewRecipeImgs(int recipeNo);
	public List<Integer> viewRecipeTagNo(int recipeNo);
	public String viewRecipeTags(int tagNo);

}
