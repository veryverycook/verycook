package net.verycook.web.recipeview;

import java.util.List;

public interface RecipeViewService {
	public RecipeViewDTO viewRecipeOne(int recipeNo);
	public List<String> viewRecipeImgs(int recipeNo);
	public List<String> viewRecipeTags(int recipeNo);
	
}
