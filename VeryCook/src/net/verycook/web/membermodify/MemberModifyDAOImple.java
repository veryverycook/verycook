package net.verycook.web.membermodify;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MemberModifyDAOImple implements MemberModifyDAO{
	@Autowired
	SqlSession sqlsession;
	//커밋을위한주석
	@Override
	public MemberModifyDTO getProfile(String MEMBER_EMAIL) {
		return sqlsession.selectOne("net.verycook.web.membermodify.getprofile", MEMBER_EMAIL);
	}

}
