package net.verycook.web.membermodify;

public class MemberModifyDTO {

	private String MEMBER_EMAIL;
	private String MEMBER_NAME;
	private String MEMBER_PASSWORD;
	private int MEMBER_SCORE;
	private String MEMBER_BIRTH;
	private char MEMBER_GENDER;
	
	public MemberModifyDTO(){}
	
	public MemberModifyDTO(String mEMBER_EMAIL, String mEMBER_NAME,
			String mEMBER_PASSWORD, int mEMBER_SCORE, String mEMBER_BIRTH,
			char mEMBER_GENDER) {
		super();
		MEMBER_EMAIL = mEMBER_EMAIL;
		MEMBER_NAME = mEMBER_NAME;
		MEMBER_PASSWORD = mEMBER_PASSWORD;
		MEMBER_SCORE = mEMBER_SCORE;
		MEMBER_BIRTH = mEMBER_BIRTH;
		MEMBER_GENDER = mEMBER_GENDER;
	}

	public String getMEMBER_EMAIL() {
		return MEMBER_EMAIL;
	}

	public void setMEMBER_EMAIL(String mEMBER_EMAIL) {
		MEMBER_EMAIL = mEMBER_EMAIL;
	}

	public String getMEMBER_NAME() {
		return MEMBER_NAME;
	}

	public void setMEMBER_NAME(String mEMBER_NAME) {
		MEMBER_NAME = mEMBER_NAME;
	}

	public String getMEMBER_PASSWORD() {
		return MEMBER_PASSWORD;
	}

	public void setMEMBER_PASSWORD(String mEMBER_PASSWORD) {
		MEMBER_PASSWORD = mEMBER_PASSWORD;
	}

	public int getMEMBER_SCORE() {
		return MEMBER_SCORE;
	}

	public void setMEMBER_SCORE(int mEMBER_SCORE) {
		MEMBER_SCORE = mEMBER_SCORE;
	}

	public String getMEMBER_BIRTH() {
		return MEMBER_BIRTH;
	}

	public void setMEMBER_BIRTH(String mEMBER_BIRTH) {
		MEMBER_BIRTH = mEMBER_BIRTH;
	}

	public char getMEMBER_GENDER() {
		return MEMBER_GENDER;
	}

	public void setMEMBER_GENDER(char mEMBER_GENDER) {
		MEMBER_GENDER = mEMBER_GENDER;
	}
	

	
}
