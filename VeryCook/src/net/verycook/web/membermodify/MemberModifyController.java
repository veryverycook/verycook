package net.verycook.web.membermodify;

import javax.servlet.http.HttpServletRequest;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MemberModifyController {

	//커밋을위한주석
	@Autowired
	MemberModifyService membermodifyservice;

	@RequestMapping(value = "/mypage/Membermodify") // 이것을 사용한다고 하면 이거 한다.
	public String Membermodify(Model model, HttpServletRequest request) {
		
		String MEMBER_EMAIL = "sunguk7704@naver.com";
		//membermodifyservice.getProfile(memberEmail);
		model.addAttribute("member", membermodifyservice.getProfile(MEMBER_EMAIL));
		System.out.println(MEMBER_EMAIL);

		/*String email = "yul2yull@gmail.com";
		model.addAttribute("member", membermodifyservice.getProfile(email));
		System.out.println("넘어옴");*/
		return "/mypage/modify"; // 이것은 끝난 후 갈 파일의 경로
	}
	
	@RequestMapping(value="/mypage/modifyOk")
	public String modifyOk(){	
		return "/mypage/modifyOk";
	}
}