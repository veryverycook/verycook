package net.verycook.web.membermodify;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MemberModifyServiceImpel implements MemberModifyService{

	@Autowired
	MemberModifyDAO membermodifydao;
	//커밋을위한주석
	@Override
	public MemberModifyDTO getProfile(String MEMBER_EMAIL) {	
		return membermodifydao.getProfile(MEMBER_EMAIL);
	}	
}
