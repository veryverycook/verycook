package net.verycook.web.admin;

public class MemberDTO {
	
	private String memberEmail;
	private String memberPassword;
	private String memberName;
	private int memberScore;
	private String memberJoindate;
	private String memberBirth;
	private char memberGender;
	private int memberStats;
	private String memberPhone;
	
	public MemberDTO() {
		// TODO Auto-generated constructor stub
	}

	public MemberDTO(String memberEmail, String memberPassword,
			String memberName, int memberScore, String memberJoindate,
			String memberBirth, char memberGender, int memberStats,
			String memberPhone) {
		super();
		this.memberEmail = memberEmail;
		this.memberPassword = memberPassword;
		this.memberName = memberName;
		this.memberScore = memberScore;
		this.memberJoindate = memberJoindate;
		this.memberBirth = memberBirth;
		this.memberGender = memberGender;
		this.memberStats = memberStats;
		this.memberPhone = memberPhone;
	}

	public String getMemberEmail() {
		return memberEmail;
	}

	public void setMemberEmail(String memberEmail) {
		this.memberEmail = memberEmail;
	}

	public String getMemberPassword() {
		return memberPassword;
	}

	public void setMemberPassword(String memberPassword) {
		this.memberPassword = memberPassword;
	}

	public String getMemberName() {
		return memberName;
	}

	public void setMemberName(String memberName) {
		this.memberName = memberName;
	}

	public int getMemberScore() {
		return memberScore;
	}

	public void setMemberScore(int memberScore) {
		this.memberScore = memberScore;
	}

	public String getMemberJoindate() {
		return memberJoindate;
	}

	public void setMemberJoindate(String memberJoindate) {
		this.memberJoindate = memberJoindate;
	}

	public String getMemberBirth() {
		return memberBirth;
	}

	public void setMemberBirth(String memberBirth) {
		this.memberBirth = memberBirth;
	}

	public char getMemberGender() {
		return memberGender;
	}

	public void setMemberGender(char memberGender) {
		this.memberGender = memberGender;
	}

	public int getMemberStats() {
		return memberStats;
	}

	public void setMemberStats(int memberStats) {
		this.memberStats = memberStats;
	}

	public String getMemberPhone() {
		return memberPhone;
	}

	public void setMemberPhone(String memberPhone) {
		this.memberPhone = memberPhone;
	}
	
	
	
	}
