package net.verycook.web.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class AdminController {
	
	@Autowired
	MemberDAO dao;
	
	@RequestMapping(value="/admin/admin")
	public String admin(){
		
		return	"/admin/admin";
	}
	
	@RequestMapping(value="/admin/adminMemberList")
	public String adminList(@RequestParam("searchEmail")String searchEmail,Model model){
		String search=searchEmail;
		search="%"+search+"%";;
	
		
		model.addAttribute("list", dao.selectAllMember(search));
		return "/admin/adminMemberList";
	}
	@RequestMapping(value="/admin/memberDelete")
	public String memberDelete(@RequestParam("memberEmail")String memberEmail){
		dao.deleteEmail(memberEmail);
		return "redirect:/admin/adminMemberList?searchEmail=";
	}
	
	
	
	
}
