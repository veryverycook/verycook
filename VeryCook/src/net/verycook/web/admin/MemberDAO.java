package net.verycook.web.admin;

import java.util.List;



public interface MemberDAO {
	public List<MemberDTO> selectAllMember(String search);
	public void deleteEmail(String memberEmail);
}
