package net.verycook.web.admin;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDAOImple implements MemberDAO{

	@Autowired
	SqlSession sqlSession;
	
	@Override
	public List<MemberDTO> selectAllMember(String search) {
//		
		List<MemberDTO> list =sqlSession.selectList("net.verycook.web.admin.selectAll",search);
		System.out.println("list ==="+list);
		return list;
	}

	@Override
	public void deleteEmail(String memberEmail) {
		sqlSession.delete("net.verycook.web.admin.deleteEmail", memberEmail);
	}

}
